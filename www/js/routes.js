angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

      .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('login', {
    url: '/page6',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('tabsController.urgencias', {
    url: '/page6',
    views: {
      'tab1': {
        templateUrl: 'templates/urgencias.html',
        controller: 'urgenciasCtrl'
      }
    }
  })

  .state('tabsController.triage', {
    url: '/page7',
    views: {
      'tab2': {
        templateUrl: 'templates/triage.html',
        controller: 'triageCtrl'
      }
    }
  })

  .state('tabsController.camino', {
    url: '/page8',
    views: {
      'tab3': {
        templateUrl: 'templates/camino.html',
        controller: 'caminoCtrl'
      }
    }
  })

  .state('tabsController.rafaelAlvarado', {
    url: '/page9',
    views: {
      'tab1': {
        templateUrl: 'templates/rafaelAlvarado.html',
        controller: 'rafaelAlvaradoCtrl'
      }
    }
  })

  .state('tabsController.reneGuevara', {
    url: '/page11',
    views: {
      'tab1': {
        templateUrl: 'templates/reneGuevara.html',
        controller: 'reneGuevaraCtrl'
      }
    }
  })

  .state('tabsController.nicolSEscorcia', {
    url: '/page12',
    views: {
      'tab1': {
        templateUrl: 'templates/nicolSEscorcia.html',
        controller: 'nicolSEscorciaCtrl'
      }
    }
  })

  .state('tabsController.lauraCasas', {
    url: '/page13',
    views: {
      'tab1': {
        templateUrl: 'templates/lauraCasas.html',
        controller: 'lauraCasasCtrl'
      }
    }
  })

  .state('tabsController.silviaRodriguez', {
    url: '/page14',
    views: {
      'tab1': {
        templateUrl: 'templates/silviaRodriguez.html',
        controller: 'silviaRodriguezCtrl'
      }
    }
  })

  .state('olvidasteTuContraseA', {
    url: '/page10',
    templateUrl: 'templates/olvidasteTuContraseA.html',
    controller: 'olvidasteTuContraseACtrl'
  })

  .state('ayuda', {
    url: '/page15',
    templateUrl: 'templates/ayuda.html',
    controller: 'ayudaCtrl'
  })

  .state('legal', {
    url: '/page16',
    templateUrl: 'templates/legal.html',
    controller: 'legalCtrl'
  })

  .state('crearCuenta', {
    url: '/page17',
    templateUrl: 'templates/crearCuenta.html',
    controller: 'crearCuentaCtrl'
  })

  .state('editarPerfil', {
    url: '/page18',
    templateUrl: 'templates/editarPerfil.html',
    controller: 'editarPerfilCtrl'
  })

$urlRouterProvider.otherwise('/page6')


});